# BenderAlgo

Bender-based utils imported from Erasmus.

# How to use the package:

```
lb-dev Bender/latest
cd BenderDev_xxxx
git clone --recurse-submodules ssh://git@gitlab.cern.ch:7999/bsm-fleet/BenderAlgo.git
mv BenderAlgo/Phys .;mv BenderAlgo/Math .;rm -rf BenderAlgo
make install
```
