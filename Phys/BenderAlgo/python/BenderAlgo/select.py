def selectVertexMin( pvs, func, cond ):
    l = []
    for pv in pvs:
        if not cond(pv): continue
        l.append([func(pv),pv])
    if not l: return None
    l.sort()
    return l[0][1]
