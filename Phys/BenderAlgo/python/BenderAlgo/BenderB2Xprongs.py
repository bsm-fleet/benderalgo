 #! /usr/bin/env python
import isolation
from SomeUtils.alyabar import *   
from LinkerInstances.eventassoc import * 
from Bender.MainMC import *                

import GaudiPython
import GaudiKernel
import cPickle

selectVertexMin = LoKi.SelectVertex.selectMin
GBL = GaudiPython.gbl.LHCb
units = GaudiKernel.SystemOfUnits
c_light = 2.99792458e+8 * units.m /units.s
light_cte = 1000./c_light
muMass = 105.6583567

class B2Xprong(AlgoMC):
    
    def analyse(self):
        
        TES = appMgr().evtsvc()
        

        self.COUNTER["EVT"] += 1
        
        self.setFilterPassed( False )

        if self.DEBUG: print self.decayname, self.COUNTER["EVT"], self.DEBUG
        
        TES = appMgr().evtsvc()
        pvs = self.vselect("pvs", ISPRIMARY)
        tracks = TES['Rec/Track/Best']
        
        self.plot(pvs.size(),"Event - primary vertices",0,20,20)
        self.plot(tracks.size(),"Event - best tracks",0,500,500)
        
        if self.DEBUG and pvs.empty() and TES["Rec/Vertex/Primary"].size() : print "pvs.empty() but PV's in Vertex/Primary"
        if pvs.empty():
            if self.DEBUG: "PVs empty"
            return SUCCESS
        bsloop = TES["/Event/"+self.LookIn+"/Particles"]
        if not bsloop :
            if self.DEBUG: print "Nothing to do", self.name()
            return SUCCESS
        
        bsloop = bsloop.containedObjects()
        self.plot(bsloop.size(),"B candidates ",0.,40.,40)
        
        if not bsloop.size(): return SUCCESS

        self.plot(pvs.size(),"B Event - primary vertices",0,20,20)
        self.plot(tracks.size(),"B Event - best tracks",0,500,500)
       
        ips2cuter = MIPCHI2(pvs,self.geo())

       
        odin = TES["DAQ/ODIN"]
        runNum,evtNum = odin.runNumber(), odin.eventNumber()
        
        if self.MC_INFO or not ("bunchCrossingType") in dir(odin):
            ###print "oooooo xtype = -1 "
            xtype = -1.
            ptype = TES["Gen/Header"].collisions()[0].processType()
            mcpv = TES["Gen/Header"].numOfCollisions()
            bid = -1
            
            
        else:
            bid = float(odin.bunchId())
            xtype = float(odin.bunchCrossingType())
            ptype = -1.
            mcpv = -1.
            
   
            
        longTracks = [] ### For isolation
        for track in tracks:
            if track.type() == 3: longTracks.append(track)

        Done = {}   ### This is the dictionary with some objects that might be passed to the functions in the final loop. NOTE that it is defined BEFORE the bsloop
        Done["TES"] = TES
        if self.MC_INFO: Done["link"]= linkedTo(GBL.MCParticle, GBL.Track, "Rec/Track/Best")
        
        for b in bsloop:
            if not b.endVertex():
                if self.DEBUG: "b does not have a end Vertex"
                self.COUNTER["weird"] +=1
                continue
            CandidateInfo = {}
            
            daughters = []
            muDOCAS = {}
            for i in range(self.Nprong):
                Done["mu" + str(i+1)] = b.daughters().at(i)
                daughters.append(b.daughters().at(i))
                muDOCAS[i] = CLAPP (daughters[i], self.geo())
           
            self.COUNTER["CandidatesAnalyzed"] += 1
            PVips2 = VIPCHI2( b, self.geo() )
            PV = selectVertexMin( pvs, PVips2, (PVips2 >= 0. ) )
            if not PV:
                if self.DEBUG: print "no PV there!!!!! exit"
                self.COUNTER["weird"] +=1
                continue

            Dis2 = VDCHI2( PV )
            Done["Candidate"], Done["PV"] =  b, PV
            
            PVvec = vector( VX(PV), VY(PV), VZ(PV) )
            SVvec = vector( VX(b.endVertex()), VY(b.endVertex()), VZ(b.endVertex()) )
        
            rSV = SVvec - PVvec
            if rSV[2] <0:
                if self.DEBUG: "negative DZ"
                self.COUNTER["weird"] +=1
                continue
            
            Bmass = M(b)
            sigDOFS = Dis2(b)
            Bips2 = PVips2(PV)
            ipscheck = min(sigDOFS,Bips2)
            if ipscheck < 0:
                if self.DEBUG: print "error on IPS calculations"
                self.COUNTER["negSq"] +=1
                ### continue, Let's decide what to do afterwards...
            
            sigDOFS = psqrt(sigDOFS)

            isoProtos = []

            for proto in TES["/Event/Phys/StdNoPIDsPions/Particles"]: isoProtos.append(proto)
            
            isolations, isolations5 = isolation.compatibleVertices2(daughters, isoProtos,  PVvec, pvs, SVvec,self,ips2cuter)

            # Marco quantities ###

            mippvs = MIP( pvs, self.geo() )
            iptoPV = IP (PV, self.geo())
            dirAngle = DIRA (PV)
            ctau = CTAU (PV)
            Bip_ = iptoPV (b)
            DirAngle_ = acos(dirAngle (b))
           
            
            Blife_ = ctau(b)*light_cte  #in picosecond
            
            CandidateInfo = {}
            CandidateInfo["evt"] = self.COUNTER["EVT"] + self.evt_of
            CandidateInfo["ipscheck"] = ipscheck
            CandidateInfo["longTrancks"] = len(longTracks)
            CandidateInfo["xtype"] = xtype
            CandidateInfo["mcpv"] = mcpv
            CandidateInfo["ptype"] = ptype
            CandidateInfo["SV1"] = VX(b.endVertex())
            CandidateInfo["SV2"] = VY(b.endVertex())
            CandidateInfo["SV3"] = VZ(b.endVertex())
            CandidateInfo["PV1"] = VX(PV)
            CandidateInfo["PV2"] = VY(PV)
            CandidateInfo["PV3"] = VZ(PV)
            CandidateInfo["nTrBestPV"] = PV.tracks().size()
            CandidateInfo["Bdissig"] = sigDOFS
            CandidateInfo["Bpt"] = PT(b)          
            CandidateInfo["Bips"] = psqrt(Bips2)
            CandidateInfo["Bmip"] = mippvs(b)
            CandidateInfo["Vchi2"] = VCHI2(b.endVertex())
            CandidateInfo["PVchi2"] = PV.chi2()
            CandidateInfo["Bmass"] = Bmass
            CandidateInfo["evtNum"] = evtNum
            CandidateInfo["runNum"] = runNum
            CandidateInfo["Blife_ps"] = Blife_
            CandidateInfo["Blife_ctau"] = ctau(b)
            CandidateInfo["Bip"] = Bip_
            CandidateInfo["angle"] = DirAngle_
            CandidateInfo["bid"] = bid
            CandidateInfo["NPVs"] = TES["Rec/Vertex/Primary"].size()
            CandidateInfo['Bptot'] = P(b)
            CandidateInfo['Bpt'] = PT(b)
            CandidateInfo['Bdis'] = vmod(rSV)
            d_ips = []
            d_ip = []
            for i in range(self.Nprong):
                thing = b.daughters().at(i)
                thingname = "mu" + str(i)# + "_"
                CandidateInfo[thingname + "iso"] = isolations[i]
                CandidateInfo[thingname + "iso5"] = isolations5[i]
                CandidateInfo[thingname + "ismu"] = ISMUON(thing)
                CandidateInfo[thingname + "p1"] = PX(thing)
                CandidateInfo[thingname + "p2"] = PY(thing)
                CandidateInfo[thingname + "p3"] = PZ(thing)
                CandidateInfo[thingname + "o1"] = thing.proto().track().position().x()
                CandidateInfo[thingname + "o2"] = thing.proto().track().position().y()
                CandidateInfo[thingname + "o3"] = thing.proto().track().position().z()
                CandidateInfo[thingname + "PIDmu"] = PIDmu(thing)
                CandidateInfo[thingname + "PIDK"] = PIDK(thing)
                CandidateInfo[thingname + "PIDe"] = PIDe(thing)
                CandidateInfo[thingname + "PIDp"] = PIDp(thing)
                CandidateInfo[thingname + "ID"] = ID(thing)
                CandidateInfo[thingname + "ttype"] = thing.proto().track().type()
                CandidateInfo[thingname + "mips"] = psqrt(ips2cuter(thing))
                CandidateInfo[thingname + "mip"] = mippvs(thing)
                CandidateInfo[thingname + "ip"] = iptoPV(thing)
                CandidateInfo[thingname + "track_Chi2"] = TRCHI2(thing)
                CandidateInfo[thingname + "track_Chi2DoF"] = TRCHI2DOF(thing)
                KULLBACK = TINFO ( LHCb.Track.CloneDist , 1.e+9 )
                CandidateInfo[thingname + "track_CloneDist"] = KULLBACK(thing)
                CandidateInfo[thingname + "ptot"] = P(thing)
                CandidateInfo[thingname + "pt"] = PT(thing)
                d_ips.append(CandidateInfo[thingname + "mips"])
                d_ip.append(CandidateInfo[thingname + "mip"])
                
                for j in range(i+1, self.Nprong):
                    
                    CandidateInfo[thingname + "mu" + str(j) + "_DOCA"] = muDOCAS[i](daughters[j])
                    CandidateInfo[thingname + "mu" + str(j) + "_mass"] = sqrt(abs(IM2(vector(PX(thing),PY(thing),PZ(thing)),vector(PX(daughters[j]),PY(daughters[j]),PZ(daughters[j])), muMass, muMass)))
               
                if self.MC_INFO :
                    mcID=-1.
                    mcMotherID=-1.
                    mcMotherKey=-1.
                    if self.MC_INFO: 

                        link= linkedTo(GBL.MCParticle, GBL.Track, "Rec/Track/Best")
                        mcParticle = link.first(thing.proto().track())
                        if mcParticle:
                            mcID=MCABSID(mcParticle)
                            mcMo=mcParticle.mother()
                            if mcMo:
                            #print "mcMo"
                                mcMotherID=MCABSID(mcMo)
                            #print "mcMo ID ",mcMotherID
                                mcMotherKey=mcMo.key()

                        CandidateInfo[thingname + "mcID"] = float(mcID)
                        CandidateInfo[thingname + "mcMotherID"] = float(mcMotherID)
                        CandidateInfo[thingname + "mcMotherKey"] = float(mcMotherKey)


            CandidateInfo["lessIPSmu"] = min(d_ips)
            CandidateInfo["lessIPmu"] = min(d_ip)
            

            for function in self.extraFunctions:
                function(self, CandidateInfo, Done)
           
            if self.TUP:
                tup = self.nTuple( self.name() )
                for key in CandidateInfo.keys():
                    tup.column(key,CandidateInfo[key])
                tup.write()
            if self.DST:
                self.addedKeys = CandidateInfo.keys()
                self.addedKeys.sort()
                for i in range(len(self.addedKeys)):
                    j = 500 + i
                    b.addInfo(j,CandidateInfo[self.addedKeys[i]])
                    
        self.setFilterPassed( True )            
                
        ## This is outside the loop !!
       
        if self.DEBUG: print "Preselected "+self.decayname
        return SUCCESS

    def finalize(self):
        print self.decayname + " counter. \n -----------"
        print self.COUNTER
        if self.DST:
            print "[||] Keys for added Info", self.name()
            for i in range(len(self.addedKeys)):
                print 500+i, self.addedKeys[i]
        print "../../../../../"
        return AlgoMC.finalize ( self ) 


