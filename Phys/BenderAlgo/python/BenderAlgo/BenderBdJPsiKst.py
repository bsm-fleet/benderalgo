#! /usr/bin/env python
from ROOT import *
import climbing
from SomeUtils.alyabar import *   
from LinkerInstances.eventassoc import * 
import GaudiKernel
from Bender.MainMC import *                
import GaudiPython
from select import selectVertexMin
#selectVertexMin = LoKi.SelectVertex.selectMin

###Defining some variables
GBL = GaudiPython.gbl.LHCb
KMASS = 493.677
MJPsi = 3096.920
units = GaudiKernel.SystemOfUnits
c_light = 2.99792458e+8 * units.m /units.s
light_cte = 1000./c_light


class BdJPsiKstar(AlgoMC):            
    def analyse(self):
        self.COUNTER["EVT"] += 1
        TES = appMgr().evtsvc()
         
        bloop = TES["/Event/"+self.LookIn+"/Particles"]
        if not bloop :
            if self.DEBUG: 
                print "No /Event/"+self.LookIn+"/Particles"
                print "Nothing to do", self.name()
            return SUCCESS
        bloop = bloop.containedObjects()
        if not bloop.size(): 
            if self.DEBUG: print "Container found but empty", self.name()
            return SUCCESS
        
        pvs_ = self.vselect("pvs_", ISPRIMARY)
        if not pvs_.size(): 
            if self.DEBUG : print "No PV's ", self.name()
            return SUCCESS
       
        ips2cuter = MIPCHI2(pvs_,self.geo())
        Done = {}   ### This is the dictionary with some objects that might be passed to the functions in the final loop. NOTE that it is defined BEFORE the bsloop
        Done["TES"] = TES
        odin = TES["DAQ/ODIN"]
        runNum,evtNum = odin.runNumber(), odin.eventNumber()
        
        
        if self.MC_INFO or not ("bunchCrossingType") in dir(odin):
            ###print "oooooo xtype = -1 "
            xtype = -1.
            ptype = TES["Gen/Header"].collisions()[0].processType()
            mcpv = TES["Gen/Header"].numOfCollisions()
            bid = -1
            
            
        else:
            bid = float(odin.bunchId())
            xtype = float(odin.bunchCrossingType())
            ptype = -1.
            mcpv = -1.
            
        if self.DEBUG: print "outside loop"
        for b in bloop:
            if self.DEBUG : print "inside loop"
            #if abs(b.particleID().pid())!=511: continue
            if "target" in dir(b.daughters().at(0)): par1= b.daughters().at(0).target()
            else: par1=b.daughters().at(0)
            if "target" in dir(b.daughters().at(1)): par2 = b.daughters().at(1).target() # Bu Daughters
            else: par2 = b.daughters().at(1) # Bu Daughters
            if ABSID(par1) == 313: kstar, jpsi = par1, par2
            else: kstar, jpsi = par2, par1

            if "target" in dir(kstar.daughters().at(0)): d1= kstar.daughters().at(0).target()
            else: d1=kstar.daughters().at(0)
            if "target" in dir(kstar.daughters().at(1)): d2 = kstar.daughters().at(1).target()
            else: d2 = kstar.daughters().at(1)

            if ABSID(d1) == 321: kaon, pion = d1, d2
            else: kaon, pion = d2, d1
            
            if not ISLONG(kaon):
                print "Something suspicious in kaon track ", self.name()
                continue
            mu1, mu2 = jpsi.daughters().at(0), jpsi.daughters().at(1) # muons
            key1 = mu1.proto().track().key()
            key2 = mu2.proto().track().key()
            keyk = kaon.proto().track().key()
            keyp = pion.proto().track().key()
            if keyk == key1: continue
            if keyk == key2: continue
            if keyp == key1: continue
            if keyp == key2: continue
            
            PVips2 = VIPCHI2( b, self.geo())
            JPsiIps2 = VIPCHI2(jpsi, self.geo())
            PV = selectVertexMin(pvs_, PVips2, (PVips2 >= 0. ))
           
            PVj = selectVertexMin(pvs_, JPsiIps2, (JPsiIps2 >= 0. ))

            if "target" in dir(mu1): mu1DOCA = CLAPP (mu1.target(), self.geo())
            else: mu1DOCA = CLAPP (mu1, self.geo())
            if "target" in dir(mu2): mu2DOCA = CLAPP (mu2.target(), self.geo())
            else: mu2DOCA = CLAPP (mu2, self.geo())
           
            kaonDOCA = CLAPP (kaon, self.geo())
            kstDOCA = CLAPP(kstar, self.geo())
            
            if not PV:
                if self.DEBUG: print "no PV there!!!!! exit"
                self.COUNTER["weird"] +=1
                continue
            if not PVj:
                if self.DEBUG: print "no PV there!!!!! exit"
                self.COUNTER["weird"] +=1
                continue
            
            Done["refittedPV"] = PV.clone()
            self.PVRefitter.remove(b, Done["refittedPV"])

            Done["refittedPVj"] = PVj.clone()
            self.PVRefitter.remove(jpsi, Done["refittedPVj"])

            ctau = CTAU (PV)
            ctau_r = CTAU (Done["refittedPV"])
            Blife_ = ctau(b)
            Blife_ps = Blife_*light_cte  #in picosecond
            Dis2 = VDCHI2( PV ) ###` To PV which minimizes B IPS
            Dis2j = VDCHI2( PVj ) ### """ JPsi IPS
            Dis2_r = VDCHI2(Done["refittedPV"])
            Dis2j_r = VDCHI2(Done["refittedPVj"])
            
        
            PVvec = vector( VX(PV), VY(PV), VZ(PV) )
            SVvec = vector( VX(b.endVertex()), VY(b.endVertex()), VZ(b.endVertex()) )
            rSV = SVvec - PVvec
            
            ## if rSV[2] <0:
##                 if self.DEBUG: "negative DZ"
##                 self.COUNTER["weird"] +=1
##                 continue
        
            Bp = vector(PX(b), PY(b), PZ(b))
            Jp = vector(PX(jpsi),PY(jpsi),PZ(jpsi))
            kp = vector(PX(kaon),PY(kaon),PZ(kaon))
       
            Bpt = vtmod(Bp)
            Bips2 = PVips2(PV)
            JPips = psqrt(JPsiIps2(PV))
            f_jpsip = dpr(PVvec,SVvec, Jp)
            Bip = dpr(PVvec, SVvec, Bp)
            
            fIPS = Bip*JPips/f_jpsip ## fake IPS for systematic Studies
            
            mu1ips2, mu2ips2, kips2,pips2,kstips2 = ips2cuter(mu1), ips2cuter(mu2), ips2cuter(kaon), ips2cuter(pion), ips2cuter(kstar)
            sigDOFS = Dis2(b)
         
            
            dDsig = Dis2(jpsi)

            ipscheck = min(sigDOFS, mu1ips2, mu2ips2, Bips2,kips2,pips2, kstips2, dDsig)
            if ipscheck < 0:
                if self.DEBUG: print "error on IPS calculations"
                self.COUNTER["negSq"] +=1
                ### continue, Let's decide what to do afterwards..

            sigDOFS =psqrt(sigDOFS)
            dDsig = psqrt(dDsig)
            Bips = psqrt(Bips2)
            jpsi_dis = psqrt(Dis2j(jpsi))
            
            
            track1 = mu1.proto().track()
            track2 = mu2.proto().track()
            o1, o2 = track1.position(), track2.position()
            mippvs = MIP( pvs_, self.geo() )
            iptoPV = IP (PV, self.geo())

            l0 = [E(kaon), vector(PX(kaon),PY(kaon),PZ(kaon))]
            l1 = [E(pion), vector(PX(pion),PY(pion),PZ(pion))]
            l2 = [E(mu1), vector(PX(mu1),PY(mu1),PZ(mu1))]
            l3 = [E(mu2), vector(PX(mu2),PY(mu2),PZ(mu2))]
            
            
            mjpsik = psqrt(IM2( l2[1]+l3[1],l0[1],3096.916,493.677))
            Th1, Th2, Phi = P_VV_angles(l0,l1,l2,l3)
            Th1P, Th2P, PhiP =  JpsiKst_Angles(l0,l1,l2,l3)
            
            mu1ippvs_ = mippvs (mu1) # IP
            mu2ippvs_ = mippvs (mu2)
            mu1ip_ = iptoPV (mu1)
            mu2ip_ = iptoPV (mu2)
            if self.MC_INFO: Done["link"]= linkedTo(GBL.MCParticle, GBL.Track, "Rec/Track/Best")

            Done["Candidate"], Done["mu1"], Done["mu2"], Done["kaon"], Done["pion"] = b, mu1, mu2, kaon ,pion
            
            CandidateInfo = {}
            CandidateInfo["evt"] = self.COUNTER["EVT"] + self.evt_of
            CandidateInfo["max_DOCA"] = max(mu1DOCA(mu2),mu1DOCA(pion),mu1DOCA(kaon), mu2DOCA(kaon),mu2DOCA(pion), kaonDOCA(pion))
            CandidateInfo["B_DOCA"] = kstDOCA(jpsi)
            CandidateInfo["Kst_DOCA"] = kaonDOCA(pion)
            CandidateInfo["Mjpsik"] = mjpsik
            CandidateInfo["ipscheck"] = ipscheck
            CandidateInfo["JPsiChi2"] = jpsi.endVertex().chi2()        
            CandidateInfo["Vchi2"] = VCHI2(b.endVertex())
            CandidateInfo["Bip_r"], CandidateInfo["Bips_r"] = Double(0.), Double(0.)
            self.Geom.distance(b, Done["refittedPV"],CandidateInfo["Bip_r"], CandidateInfo["Bips_r"])
            CandidateInfo["Bips_r"] = float(psqrt(CandidateInfo["Bips_r"]))
            CandidateInfo["Bip_r"] = float(CandidateInfo["Bip_r"])
            CandidateInfo["Th1"] = Th1
            CandidateInfo["Th2"] = Th2
            CandidateInfo["Phi"] = Phi
            CandidateInfo["Th1P"] = Th1P
            CandidateInfo["Th2P"] = Th2P
            CandidateInfo["PhiP"] = PhiP
            CandidateInfo["KstChi2"] = VCHI2(kstar.endVertex())
            CandidateInfo["Kst_pt"] = PT(kstar)
            CandidateInfo["Jpsi_pt"] = PT(jpsi)
            CandidateInfo["pion_pt"] = PT(pion)
            CandidateInfo["kaon_pt"] = PT(kaon)
            CandidateInfo["Bmass_JC"] = DTF_FUN(M,True,"J/psi(1S)")(b)
            CandidateInfo["k1ips"] = psqrt(kips2)
            CandidateInfo["p1ips"] = psqrt(pips2)
            CandidateInfo["mu1ips"] = psqrt(mu1ips2)
            CandidateInfo["mu2ips"] = psqrt(mu2ips2)
            CandidateInfo["mu1mip"] = mu1ippvs_
            CandidateInfo["mu2mip"] = mu2ippvs_
            CandidateInfo["mu1ip"] = mu1ip_
            CandidateInfo["mu2ip"] = mu2ip_
            CandidateInfo["Kstips"] = psqrt(kstips2)
            CandidateInfo["JPsiMass"] = M(jpsi)
            CandidateInfo["B_ctau"] = Blife_ ## in milimeters !!!!!
            CandidateInfo["Blife_ps"] = Blife_ps  ### in ps !!
            CandidateInfo["JPsi_Dsig"] = jpsi_dis
            CandidateInfo["KstMass"] = M(kstar)
            CandidateInfo["Bmass"] = M(b)
            CandidateInfo["Bpt"] = Bpt
            CandidateInfo["xtype"] = xtype
            CandidateInfo["bid"] = bid
            CandidateInfo["mu1ismu"] = ISMUON(mu1)
            CandidateInfo["mu2ismu"] = ISMUON(mu2)

            if "target" in dir(mu1): muDOCA = CLAPP (mu1.target(), self.geo())
            else: muDOCA = CLAPP (mu1, self.geo())

            CandidateInfo["DOCA"] = muDOCA(mu2)
            CandidateInfo["longTracks"] = len(TES['Rec/Track/Best'])
            CandidateInfo["Blife_ps_r"] = ctau_r(b)*light_cte
            CandidateInfo["Bdissig_r"] = psqrt(Dis2_r(b))
            CandidateInfo["dDsig_r"] = psqrt(Dis2_r(jpsi))

            CandidateInfo["JPsi_Dsig_r"] = psqrt(Dis2j_r(jpsi))
            
            CandidateInfo["k1ptot"] = P(kaon)
            CandidateInfo["k1_eta"] = ETA(kaon)
            CandidateInfo["pionptot"] = P(pion)
            CandidateInfo["pion_eta"] = ETA(pion)
            CandidateInfo["mu1p1"] = PX(mu1)
            CandidateInfo["mu1p2"] = PY(mu1)
            CandidateInfo["mu1p3"] = PZ(mu1)
            CandidateInfo["mu2p1"] = PX(mu2)
            CandidateInfo["mu2p2"] = PY(mu2)       
            CandidateInfo["mu2p3"] = PZ(mu2)
            CandidateInfo["mu1o1"] = o1.x()
            CandidateInfo["mu1o2"] = o1.y()
            CandidateInfo["mu1o3"] = o1.z()
            CandidateInfo["mu2o1"] = o2.x()
            CandidateInfo["mu2o2"] = o2.y()
            CandidateInfo["mu2o3"] = o2.z()
            CandidateInfo["k1p1"] = PX(kaon)
            CandidateInfo["k1p2"] = PY(kaon)
            CandidateInfo["k1p3"] = PZ(kaon)
            CandidateInfo["p1p1"] = PX(pion)
            CandidateInfo["p1p2"] = PY(pion)
            CandidateInfo["p1p3"] = PZ(pion)
            CandidateInfo["QK"] = Q(kaon)          
            CandidateInfo["PIDmu1"] = PIDmu(mu1)   
            CandidateInfo["PIDmu2"] = PIDmu(mu2)   
            CandidateInfo["PIDk"] = PIDK(kaon)    
            CandidateInfo["PIDkp"] = PIDK(kaon) - PIDp(kaon)
            CandidateInfo["PIDpi"] = PIDK(pion)   
            CandidateInfo["kaon_PROBNNk"] = PROBNNk(kaon)
            CandidateInfo["pion_PROBNNk"] = PROBNNk(pion)
            CandidateInfo["pion_PROBNNpi"] = PROBNNpi(pion)
            
            CandidateInfo["SV1"] = VX(b.endVertex())           
            CandidateInfo["SV2"] = VY(b.endVertex())           
            CandidateInfo["SV3"] = VZ(b.endVertex())          
            CandidateInfo["evtNum"] = TES["Rec/Header"].evtNumber()
            CandidateInfo["runNum"] = TES["Rec/Header"].runNumber()          
            CandidateInfo["PV1"] = VX(PV)
            CandidateInfo["PV2"] = VY(PV)
            CandidateInfo["PV3"] = VZ(PV)
            CandidateInfo["Bips"] = Bips
            CandidateInfo["fIPS"] = fIPS
            CandidateInfo["B_IP" ] =  Bip
            CandidateInfo["Bdissig"]= sigDOFS
            CandidateInfo["dDsig"] = dDsig
            CandidateInfo["B_pt"] = CandidateInfo["Bpt"]
            CandidateInfo["lessIPS"] = min(CandidateInfo["k1ips"], CandidateInfo["p1ips"] , CandidateInfo["mu1ips"], CandidateInfo["mu2ips"] )

            CandidateInfo["mu1_track_Chi2"] = TRCHI2 (mu1)
            CandidateInfo["mu2_track_Chi2"] = TRCHI2 (mu2)

            CandidateInfo["mu1_track_Chi2DoF"] = TRCHI2DOF (mu1)
            CandidateInfo["mu2_track_Chi2DoF"] = TRCHI2DOF (mu2)

            CandidateInfo["kaon_track_Chi2"] = TRCHI2 (kaon)
            CandidateInfo["pion_track_Chi2"] = TRCHI2 (pion)

            CandidateInfo["mu1_kaon_Chi2DoF"] = TRCHI2DOF (kaon)
            CandidateInfo["pion_track_Chi2DoF"] = TRCHI2DOF (pion)

            KULLBACK = TINFO ( LHCb.Track.CloneDist , 1.e+9 )
            CandidateInfo["mu1_track_CloneDist"] = KULLBACK(mu1)
            CandidateInfo["mu2_track_CloneDist"] = KULLBACK(mu2)
            CandidateInfo["kaon_track_CloneDist"] = KULLBACK(kaon)            
            CandidateInfo["pion_track_CloneDist"] = KULLBACK(pion)
            CandidateInfo["LF_time"], CandidateInfo["LF_terr"], CandidateInfo["LF_tchi2"] = Double(0.), Double(0.), Double(0.)
            sc = self.LifeFitter.fit ( Done["refittedPV"], b , CandidateInfo["LF_time"],  CandidateInfo["LF_terr"], CandidateInfo["LF_tchi2"] )

            CandidateInfo["LF_time"] = CandidateInfo["LF_time"]*1000
            CandidateInfo["LF_terr"] = CandidateInfo["LF_terr"]*1000
            theDira = DIRA(Done["refittedPV"])
            CandidateInfo["DIRA"] = theDira(b)

            
            for function in self.extraFunctions:
                function(self, CandidateInfo, Done)
            if self.DEBUG: print "Tuple option on? ", self.TUP
            if self.TUP:
                tup = self.nTuple( self.name() )
                for key in CandidateInfo.keys():
                    tup.column(key,CandidateInfo[key])
                if self.DEBUG: print self.name(), " : Writting NTuple ", tup.GetName()
                tup.write()
                
            if self.DST:
                self.addedKeys = CandidateInfo.keys()
                self.addedKeys.sort()
                for i in range(len(self.addedKeys)):
                    j = 500 + i
                    b.addInfo(j,CandidateInfo[self.addedKeys[i]])
                    
        self.setFilterPassed( True )
        if self.DEBUG: print "Bender has found a Bd --> J/psi K*0 candidate"
        self.COUNTER["Sel"] += 1
        return SUCCESS
    
    def finalize(self):
        print "Bd --> J/psi K*0 counter. \n -----------"
        print self.COUNTER
        if self.DST:
            print "[||] Keys for added Info", self.name()
            for i in range(len(self.addedKeys)):
                print 500+i, self.addedKeys[i]
        print "../../../../../"
        return AlgoMC.finalize(self)
