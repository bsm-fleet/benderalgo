#! /usr/bin/env python
from ROOT import *
import climbing
from SomeUtils.alyabar import *   
from LinkerInstances.eventassoc import * 
import GaudiKernel
from Bender.MainMC import *                
import GaudiPython
from select import selectVertexMin
#selectVertexMin = LoKi.SelectVertex.selectMin

###Defining some variables
GBL = GaudiPython.gbl.LHCb
KMASS = 493.677
MJPsi = 3096.920
units = GaudiKernel.SystemOfUnits
c_light = 2.99792458e+8 * units.m /units.s
light_cte = 1000./c_light


class BsJPsiPhi(AlgoMC):            
    def analyse(self):
        self.COUNTER["EVT"] += 1
        TES = appMgr().evtsvc()
         
        bloop = TES["/Event/"+self.LookIn+"/Particles"]
        if not bloop :
            if self.DEBUG: print "Nothing to do", self.name()
            return SUCCESS
        bloop = bloop.containedObjects()
        if not bloop.size(): return SUCCESS
        
        pvs_ = self.vselect("pvs_", ISPRIMARY)
        if not pvs_.size(): return SUCCESS
       
        ips2cuter = MIPCHI2(pvs_,self.geo())
        Done = {}   ### This is the dictionary with some objects that might be passed to the functions in the final loop. NOTE that it is defined BEFORE the bsloop
        Done["TES"] = TES
        odin = TES["DAQ/ODIN"]
        runNum,evtNum = odin.runNumber(), odin.eventNumber()
        
        
        if self.MC_INFO or not ("bunchCrossingType") in dir(odin):
            ###print "oooooo xtype = -1 "
            xtype = -1.
            ptype = TES["Gen/Header"].collisions()[0].processType()
            mcpv = TES["Gen/Header"].numOfCollisions()
            bid = -1
            
            
        else:
            bid = float(odin.bunchId())
            xtype = float(odin.bunchCrossingType())
            ptype = -1.
            mcpv = -1.
            
        
        for b in bloop:
            if "target" in dir(b.daughters().at(0)): par1= b.daughters().at(0).target()
            else: par1=b.daughters().at(0)
            if "target" in dir(b.daughters().at(1)): par2 = b.daughters().at(1).target() # Bu Daughters
            else: par2 = b.daughters().at(1) # Bu Daughters

            if ABSID(par1) == 333: phi, jpsi = par1, par2
            else: phi, jpsi = par2, par1

            if "target" in dir(phi.daughters().at(0)): d1= phi.daughters().at(0).target()
            else: d1=phi.daughters().at(0)
            if "target" in dir(phi.daughters().at(1)): d2 = phi.daughters().at(1).target() # Bu Daughters
            else: d2 = phi.daughters().at(1) # Bu Daughters
            #JA TODO: order by charge
            kaon1, kaon2 = d1, d2
            #if ABSID(d1) == 321: kaon, pion = d1, d2
            #else: kaon, pion = d2, d1
            
            if (not ISLONG(kaon1)) or not ISLONG(kaon2):
                print "Something suspicious in kaon track ", self.name()
                continue
            mu1, mu2 = jpsi.daughters().at(0), jpsi.daughters().at(1) # muons
            key1 = mu1.proto().track().key()
            key2 = mu2.proto().track().key()
            keyk1 = kaon1.proto().track().key()
            keyk2 = kaon2.proto().track().key()
            if keyk1 == key1: continue
            if keyk1 == key2: continue
            if keyk2 == key1: continue
            if keyk2 == key2: continue
            
            PVips2 = VIPCHI2( b, self.geo())
            JPsiIps2 = VIPCHI2(jpsi, self.geo())
            
            PV = selectVertexMin(pvs_, PVips2, (PVips2 >= 0. ))
            #PVj = self.bestVertex ( b )
            #PVj = self.bestVertex ( jpsi )
            
            PVj = selectVertexMin(pvs_, JPsiIps2, (JPsiIps2 >= 0. ))

            if not PV:
                if self.DEBUG: print "no PV there!!!!! exit"
                self.COUNTER["weird"] +=1
                continue
            if not PVj:
                if self.DEBUG: print "no PV there!!!!! exit"
                self.COUNTER["weird"] +=1
                continue
            Done["refittedPV"] = PV.clone()
            self.PVRefitter.remove(b, Done["refittedPV"])
            Done["refittedPVj"] = PVj.clone()
            self.PVRefitter.remove(jpsi, Done["refittedPVj"])

            ctau = CTAU (PV)
            ctau_r = CTAU (Done["refittedPV"])
           
            Dis2 = VDCHI2( PV ) ###` To PV which minimizes B IPS
            Dis2j = VDCHI2( PVj ) ### """ JPsi IPS
            Dis2_r = VDCHI2(Done["refittedPV"])
            Dis2j_r = VDCHI2(Done["refittedPVj"])
          
            PVvec = vector( VX(PV), VY(PV), VZ(PV) )
            SVvec = vector( VX(b.endVertex()), VY(b.endVertex()), VZ(b.endVertex()) )
            rSV = SVvec - PVvec
            
            ## if rSV[2] <0:
##                 if self.DEBUG: "negative DZ"
##                 self.COUNTER["weird"] +=1
##                 continue
        
            Bp = vector(PX(b), PY(b), PZ(b))
            Jp = vector(PX(jpsi),PY(jpsi),PZ(jpsi))
            kp = vector(PX(kaon1),PY(kaon1),PZ(kaon1))
       
            Bpt = vtmod(Bp)
            Bips2 = PVips2(PV)
            JPips = psqrt(JPsiIps2(PV))
           
            f_jpsip = dpr(PVvec,SVvec, Jp)
            Bip = dpr(PVvec, SVvec, Bp)
            
            fIPS = Bip*JPips/f_jpsip ## fake IPS for systematic Studies
            
            mu1ips2, mu2ips2, k1ips2,k2ips2,phiips2 = ips2cuter(mu1), ips2cuter(mu2), ips2cuter(kaon1), ips2cuter(kaon2), ips2cuter(phi)
            sigDOFS = Dis2(b)

            l0 = [E(kaon1), vector(PX(kaon1),PY(kaon1),PZ(kaon1))]
            l1 = [E(kaon2), vector(PX(kaon2),PY(kaon2),PZ(kaon2))]
            l2 = [E(mu1), vector(PX(mu1),PY(mu1),PZ(mu1))]
            l3 = [E(mu2), vector(PX(mu2),PY(mu2),PZ(mu2))]

            Th1, Th2, Phi = P_VV_angles(l0,l1,l2,l3)
            Th1P, Th2P, PhiP =  JpsiKst_Angles(l0,l1,l2,l3)

            
            dDsig = Dis2(jpsi)

            ipscheck = min(sigDOFS, mu1ips2, mu2ips2, Bips2,k1ips2,k2ips2, phiips2, dDsig)
            if ipscheck < 0:
                if self.DEBUG: print "error on IPS calculations"
                self.COUNTER["negSq"] +=1
                ### continue, Let's decide what to do afterwards..

            sigDOFS =psqrt(sigDOFS)
            dDsig = psqrt(dDsig)
            Bips = psqrt(Bips2)
            jpsi_dis = psqrt(Dis2j(jpsi))
            
            
            track1 = mu1.proto().track()
            track2 = mu2.proto().track()
            track3 = kaon1.proto().track()
            track4 = kaon2.proto().track()
            o1, o2 = track1.position(), track2.position()
            o3, o4 = track3.position(), track4.position()
            mippvs = MIP( pvs_, self.geo() )
            iptoPV = IP (PV, self.geo())


            mu1ippvs_ = mippvs (mu1) # IP
            mu2ippvs_ = mippvs (mu2)
            mu1ip_ = iptoPV (mu1)
            mu2ip_ = iptoPV (mu2)


            Done["Candidate"], Done["mu1"], Done["mu2"] = b, mu1, mu2
          
            CandidateInfo = {}
            CandidateInfo["evt"] = self.COUNTER["EVT"] + self.evt_of
            CandidateInfo["ipscheck"] = ipscheck
            CandidateInfo["JPsiChi2"] = jpsi.endVertex().chi2()        
            CandidateInfo["Vchi2"] = VCHI2(b.endVertex())
            CandidateInfo["k1ips"] = psqrt(k1ips2)
            CandidateInfo["k2ips"] = psqrt(k2ips2)
            CandidateInfo["mu1ips"] = psqrt(mu1ips2)
            CandidateInfo["mu2ips"] = psqrt(mu2ips2)
            CandidateInfo["mu1mip"] = mu1ippvs_
            CandidateInfo["mu2mip"] = mu2ippvs_
            CandidateInfo["mu1ip"] = mu1ip_
            CandidateInfo["k2ip"] = iptoPV(kaon2)
            CandidateInfo["k1ip"] = iptoPV(kaon1)
            CandidateInfo["mu2ip"] = mu2ip_
            CandidateInfo["Phiips"] = psqrt(phiips2)
            CandidateInfo["JPsiMass"] = M(jpsi)
            CandidateInfo["JPsi_Dsig"] = jpsi_dis
            CandidateInfo["PhiMass"] = M(phi)
            CandidateInfo["Th1"] = Th1
            CandidateInfo["Th2"] = Th2
            CandidateInfo["Phi"] = Phi
            CandidateInfo["Th1P"] = Th1P
            CandidateInfo["Th2P"] = Th2P
            CandidateInfo["PhiP"] = PhiP

            CandidateInfo["Bmass"] = M(b)
            CandidateInfo["Bpt"] = Bpt
            CandidateInfo["xtype"] = xtype
            CandidateInfo["bid"] = bid
            CandidateInfo["mu1ismu"] = ISMUON(mu1)
            CandidateInfo["mu2ismu"] = ISMUON(mu2)
            CandidateInfo["longTracks"] = len(TES['Rec/Track/Best'])
            CandidateInfo["Bmass_JC"] = DTF_FUN(M,True,"J/psi(1S)")(b)
            CandidateInfo["Bip_r"], CandidateInfo["Bips_r"] = Double(0.), Double(0.)
            self.Geom.distance(b, Done["refittedPV"],CandidateInfo["Bip_r"], CandidateInfo["Bips_r"])
            CandidateInfo["Bips_r"] = float(psqrt(CandidateInfo["Bips_r"]))
            CandidateInfo["Bip_r"] = float(CandidateInfo["Bip_r"])
            CandidateInfo["Blife_ps"] = ctau(b)*light_cte
            CandidateInfo["Blife_ps_r"] = ctau_r(b)*light_cte
            CandidateInfo["Bdissig_r"] = psqrt(Dis2_r(b))
            CandidateInfo["dDsig_r"] = psqrt(Dis2_r(jpsi))
            CandidateInfo["JPsi_Dsig_r"] = psqrt(Dis2j_r(jpsi))

            if "target" in dir(mu1): muDOCA = CLAPP (mu1.target(), self.geo())
            else: muDOCA = CLAPP (mu1, self.geo())

            CandidateInfo["DOCA"] = muDOCA(mu2)
            CandidateInfo["mu1p1"] = PX(mu1)
            CandidateInfo["mu1p2"] = PY(mu1)
            CandidateInfo["mu1p3"] = PZ(mu1)
            CandidateInfo["mu2p1"] = PX(mu2)
            CandidateInfo["mu2p2"] = PY(mu2)       
            CandidateInfo["mu1pt"] = PT(mu1)
            CandidateInfo["mu2pt"] = PT(mu2)       
            CandidateInfo["mu2p3"] = PZ(mu2)
            CandidateInfo["mu2ptot"] = P(mu2)
            CandidateInfo["mu1ptot"] = P(mu1)
            CandidateInfo["mu1o1"] = o1.x()
            CandidateInfo["mu1o2"] = o1.y()
            CandidateInfo["mu1o3"] = o1.z()
            CandidateInfo["mu2o1"] = o2.x()
            CandidateInfo["mu2o2"] = o2.y()
            CandidateInfo["mu2o3"] = o2.z()

            CandidateInfo["k1o1"] = o3.x()
            CandidateInfo["k1o2"] = o3.y()
            CandidateInfo["k1o3"] = o3.z()
            CandidateInfo["k2o1"] = o4.x()
            CandidateInfo["k2o2"] = o4.y()
            CandidateInfo["k2o3"] = o4.z()
            
            CandidateInfo["k1p1"] = PX(kaon1)
            CandidateInfo["k1p2"] = PY(kaon1)
            CandidateInfo["k1p3"] = PZ(kaon1)
            CandidateInfo["k2p1"] = PX(kaon2)
            CandidateInfo["k2p2"] = PY(kaon2)
            CandidateInfo["k2p3"] = PZ(kaon2)
            CandidateInfo["k1ptot"] = P(kaon1)
            CandidateInfo["k1_eta"] = ETA(kaon1)
            CandidateInfo["k2ptot"] = P(kaon2)
            CandidateInfo["k2_eta"] = ETA(kaon2)
            CandidateInfo["QK1"] = Q(kaon1)
            CandidateInfo["QK2"] = Q(kaon2)          
            CandidateInfo["PIDmu1"] = PIDmu(mu1)   
            CandidateInfo["PIDmu2"] = PIDmu(mu2)   
            CandidateInfo["PIDk1"] = PIDK(kaon1)    
            CandidateInfo["PIDkp1"] = PIDK(kaon1) - PIDp(kaon1)
            CandidateInfo["PIDk2"] = PIDK(kaon2)    
            CandidateInfo["PIDkp2"] = PIDK(kaon2) - PIDp(kaon2)
            CandidateInfo["SV1"] = VX(b.endVertex())           
            CandidateInfo["SV2"] = VY(b.endVertex())           
            CandidateInfo["SV3"] = VZ(b.endVertex())          
            CandidateInfo["evtNum"] = TES["Rec/Header"].evtNumber()
            CandidateInfo["runNum"] = TES["Rec/Header"].runNumber()          
            CandidateInfo["PV1"] = VX(PV)
            CandidateInfo["PV2"] = VY(PV)
            CandidateInfo["PV3"] = VZ(PV)
            CandidateInfo["Bips"] = Bips
            CandidateInfo["fIPS"] = fIPS
            CandidateInfo["Bip" ] =  Bip
            CandidateInfo["Bdissig"]= sigDOFS
            CandidateInfo["dDsig"] = dDsig

            CandidateInfo["mu1_track_Chi2"] = TRCHI2 (mu1)
            CandidateInfo["mu2_track_Chi2"] = TRCHI2 (mu2)

            CandidateInfo["mu1_track_Chi2DoF"] = TRCHI2DOF (mu1)
            CandidateInfo["mu2_track_Chi2DoF"] = TRCHI2DOF (mu2)

            CandidateInfo["kaon1_track_Chi2"] = TRCHI2(kaon1)
            CandidateInfo["kaon2_track_Chi2"] = TRCHI2(kaon2)

            CandidateInfo["kaon1_track_Chi2DoF"] = TRCHI2DOF (kaon1)
            CandidateInfo["kaon2_track_Chi2DoF"] = TRCHI2DOF (kaon2)
            CandidateInfo["kaon1_eta"] = ETA(kaon1)
            CandidateInfo["kaon2_eta"] = ETA(kaon1)
            
            KULLBACK = TINFO ( LHCb.Track.CloneDist , 1.e+9 )
            CandidateInfo["mu1_track_CloneDist"] = KULLBACK(mu1)
            CandidateInfo["mu2_track_CloneDist"] = KULLBACK(mu2)
            CandidateInfo["kaon1_track_CloneDist"] = KULLBACK(kaon1)        ##    
            CandidateInfo["kaon2_track_CloneDist"] = KULLBACK(kaon2)
            CandidateInfo["LF_time"], CandidateInfo["LF_terr"], CandidateInfo["LF_tchi2"] = Double(0.), Double(0.), Double(0.)
            sc = self.LifeFitter.fit ( Done["refittedPV"], b , CandidateInfo["LF_time"],  CandidateInfo["LF_terr"], CandidateInfo["LF_tchi2"] )

            CandidateInfo["LF_time"] = CandidateInfo["LF_time"]*1000
            CandidateInfo["LF_terr"] = CandidateInfo["LF_terr"]*1000
            theDira = DIRA(Done["refittedPV"])
            CandidateInfo["DIRA"] = theDira(b)

            for function in self.extraFunctions:
                function(self, CandidateInfo, Done)
                          
            if self.TUP:
                tup = self.nTuple( self.name() )
                for key in CandidateInfo.keys():
                    tup.column(key,CandidateInfo[key])
                tup.write()
                
            if self.DST:
                self.addedKeys = CandidateInfo.keys()
                self.addedKeys.sort()
                for i in range(len(self.addedKeys)):
                    j = 500 + i
                    b.addInfo(j,CandidateInfo[self.addedKeys[i]])
                    
        self.setFilterPassed( True )
        if self.DEBUG: print "Bender has found a "+ self.decayname+" candidate"
        self.COUNTER["Sel"] += 1
        return SUCCESS
    
    def finalize(self):
        print "Bs --> J/psi Phi counter. \n -----------"
        print self.COUNTER
        if self.DST:
            print "[||] Keys for added Info", self.name()
            for i in range(len(self.addedKeys)):
                print 500+i, self.addedKeys[i]
        print "../../../../../"
        return AlgoMC.finalize(self)
