#! /usr/bin/env python
import isolation, climbing
from SomeUtils.alyabar import *
from LinkerInstances.eventassoc import *
from Bender.MainMC import *
from math import sqrt as s_q_r_t
import GaudiPython
import GaudiKernel
#__sm_
selectVertexMin = LoKi.SelectVertex.selectMin

###Defining some variables
MBs = 5369.600
GBL = GaudiPython.gbl.LHCb
units = GaudiKernel.SystemOfUnits
c_light = 2.99792458e+8 * units.m /units.s
light_cte = 1000./c_light

class B2QQ(AlgoMC):
    
    def analyse(self):
        
        self.COUNTER["EVT"] += 1
        print self.COUNTER["EVT"]
        TES = appMgr().evtsvc()
        pvs = self.vselect("pvs", ISPRIMARY)
        if pvs.empty():
            print "( oo ) NO PV's"
            return SUCCESS
  #      bsloop = TES["/Event/Phys/"+self.LookIn+"/Particles"]
            
        muplus  = self.select ( 'mu+' , (ABSID == 'mu+') & (Q > 0))
        muminus = self.select ( 'mu-' , (ABSID == 'mu+') & (Q < 0))
        muons_fV = self.select("mfv", (ABSID ==13))   ### Fot JPsi Veto Studies
        
        ips2cuter = MIPCHI2(pvs,self.geo())

        mulist = []  ## For Bc --> J/Psi mu nu Veto

        for mu in muons_fV:
            mulist.append(mu)
            
        longTracks = [] ### For isolation
        for track in TES["Rec/Track/Best"].containedObjects():
            if track.type() == 3: longTracks.append(track)
            
        bsloop = self.loop("mu+ mu-", "B_s0")
        Done = {}   ### This is the dictionary with some objects that might be passed to the functions in the final loop. NOTE that it is defined BEFORE the bsloop

        if self.ONLY_MC:
            Bs = climbing.climbing_b2qq(TES["MC/Particles"].containedObjects(), MCID, self.mcs)
            #print "Bs", Bs
            if Bs == None: 
                print "Bs is not htere, continue"
                return SUCCESS
#   continue
            Done["mcB"] = Bs
            link = linkedTo(GBL.MCParticle, GBL.Track, "Rec/Track/Best" )
            #ev =  Bs.endVertices().at(0).products()
           
        
        Done["TES"] = TES
        if self.MC_INFO: Done["link"]= linkedTo(GBL.MCParticle, GBL.Track, "Rec/Track/Best")

        for b in bsloop:
            #print "Something"
            if not b.particle(): continue
            if not b.particle().endVertex(): continue
            mu1, mu2 = b(1), b(2)
            #print "mu1",mu1            
            #print "mu2",mu2

            if self.ONLY_MC:
                mcmu1 = link.first(mu1.proto().track())
                if not mcmu1: continue
                if not mcmu1.mother(): continue
                mcmu2 = link.first(mu2.proto().track())
                if not mcmu2: continue
                if not mcmu2.mother(): continue
                
                if mcmu1.mother().key() != Bs.key() or mcmu2.mother().key() != Bs.key() : continue

            #print 'Here'
            self.COUNTER["MuMuCouplesAnalyzed"] += 1
            PVip = VIP( b.particle(), self.geo() )
            PVips2 = VIPCHI2( b.particle(), self.geo() )
            PV = selectVertexMin( pvs, PVip, (PVip >= 0. ) )
            Dis2 = VDCHI2( PV )
            Dis = VD(PV)
            
            PVvec = vector( VX(PV), VY(PV), VZ(PV) )
            SVvec = vector( VX(b), VY(b), VZ(b) )
            Bp = vector( PX(b), PY(b), PZ(b) )
        
            rSV = SVvec - PVvec
            
            Bmass = M(b)
           
            #if abs(Bmass - MBs ) > self.MASSWINDOW: continue
            Chi2 = VCHI2(b)
            
            #if Chi2 > self.Chi2_Cut: continue
            Bips = s_q_r_t(PVips2(PV))
            #if Bips > self.Bips_Cut: continue
            sigDOFS = Dis2(b.particle())
            sigDOFS = s_q_r_t(sigDOFS)*rSV[2]/abs(rSV[2])
            sigDOF = Dis(b.particle())*rSV[2]/abs(rSV[2])
           # if sigDOFS < self.signedDOFS_Cut :continue
            Bpt  = PT(b)
           
            mu1ips, mu2ips = s_q_r_t(ips2cuter(mu1)),s_q_r_t(ips2cuter(mu2))
            lessIPSmu = min(mu1ips,mu2ips)
           
            CandidateInfo = {}
           
            mu1, mu2 = b.particle().daughters().at(0), b.particle().daughters().at(1)
           
           
            Done["Candidate"], Done["PV"], Done["mu1"], Done["mu2"] =  b.particle(), PV, mu1, mu2
          
            o1, o2 = mu1.proto().track().position(), mu2.proto().track().position()
            mus3, pts3, jpsis3 = isolation.anotherMuon_v2(mulist, b.particle())   
            pts3.sort()
            jpsis3.sort()
            isolations = isolation.compatibleVertices([mu1,mu2], longTracks, PVvec)

            # Marco quantities ###

            mippvs = MIP( pvs, self.geo() )
            iptoPV = IP(PV, self.geo())
            dirAngle = DIRA(PV)
            ctau = CTAU(PV)
            muDOCA = CLAPP(mu1.target(), self.geo())
            
            mu1ippvs_ = mippvs(mu1) # IP
            mu2ippvs_ = mippvs(mu2)
            mu1ip_ = iptoPV(mu1)
            mu2ip_ = iptoPV(mu2)
            Bip_ = iptoPV (b.particle())
            DirAngle_ = acos(dirAngle (b.particle()))
           
            
            Blife_ = ctau(b.particle())*light_cte  #in picosecond
            muonsDOCA_ = muDOCA(mu2)
            ###
            B1 = [E(mu1), vector(PX(mu1), PY(mu1), PZ(mu1))]
            B2 = [E(mu2), vector(PX(mu2), PY(mu2), PZ(mu2))]
          
            CandidateInfo = {}
            CandidateInfo["evt"] = self.COUNTER["EVT"]
            CandidateInfo["mu1iso"] = isolations[0]
            CandidateInfo["mu2iso"] = isolations[1]
            CandidateInfo["longTrancks"] = len(longTracks)
            
            CandidateInfo["mu1p1"] = PX(mu1)
            CandidateInfo["mu1p2"] = PY(mu1)
            CandidateInfo["mu1p3"] = PZ(mu1)
            CandidateInfo["mu2p1"] = PX(mu2)
            CandidateInfo["mu2p2"] = PY(mu2)
            CandidateInfo["mu2p3"] = PZ(mu2)
            CandidateInfo["mu1o1"] = o1.x() 
            CandidateInfo["mu1o2"] = o1.y() 
            CandidateInfo["mu1o3"] = o1.z() 
            CandidateInfo["mu2o1"] = o2.x() 
            CandidateInfo["mu2o2"] = o2.y() 
            CandidateInfo["mu2o3"] = o2.z() 
            CandidateInfo["PIDmu1"] = PIDmu(mu1)
            CandidateInfo["PIDmu2"] = PIDmu(mu2)
            CandidateInfo["PIDk1"] = PIDK(mu1)
            CandidateInfo["PIDk2"] = PIDK(mu2)
            CandidateInfo["PIDe1"] = PIDe(mu1)
            CandidateInfo["PIDe2"] = PIDe(mu2)
            CandidateInfo["PIDp1"] = PIDp(mu1)
            CandidateInfo["PIDp2"] = PIDp(mu2)
            CandidateInfo["SV1"] = VX(b.particle().endVertex())
            CandidateInfo["SV2"] = VY(b.particle().endVertex())
            CandidateInfo["SV3"] = VZ(b.particle().endVertex())
            CandidateInfo["PV1"] = VX(PV)
            CandidateInfo["PV2"] = VY(PV)
            CandidateInfo["PV3"] = VZ(PV)
            CandidateInfo["ID1"] = ID(mu1)
            CandidateInfo["ID2"] = ID(mu2)            
            CandidateInfo["Bdissig"] = sigDOFS
            CandidateInfo["Bdis"] = sigDOF
            CandidateInfo["Bpt"] = PT(b)          
            CandidateInfo["Bips"] = s_q_r_t(PVips2(PV))
            CandidateInfo["Vchi2"] = VCHI2(b.particle().endVertex())
            CandidateInfo["PVchi2"] = PV.chi2()
            CandidateInfo["Bmass"] = Bmass
            CandidateInfo["evtNum"] = TES["Rec/Header"].evtNumber()
            CandidateInfo["runNum"] = TES["Rec/Header"].runNumber()
            CandidateInfo["3muon"]=  mus3
            CandidateInfo["3muonMaxpt"] = pts3[-1]
            CandidateInfo["3muonMinjpsi"] =  jpsis3[0]
            CandidateInfo["mu1ips"] = s_q_r_t(ips2cuter(mu1))
            CandidateInfo["mu2ips"] = s_q_r_t(ips2cuter(mu2))
            CandidateInfo["mu1mip"] = mu1ippvs_
            CandidateInfo["mu2mip"] = mu2ippvs_
            CandidateInfo["mu1ip"] = mu1ip_
            CandidateInfo["mu2ip"] = mu2ip_
            
            CandidateInfo["mu1pt"] = PT(mu1)
            CandidateInfo["mu2pt"] = PT(mu2)
            CandidateInfo["mu1ptot"] = P(mu1)
            CandidateInfo["mu2ptot"] = P(mu2)
            CandidateInfo["mu1_eta"] = ETA(mu1)
            CandidateInfo["mu2_eta"] = ETA(mu2)
            CandidateInfo["B_eta"] = ETA(b)
            

            CandidateInfo["Blife_ps"] = Blife_
            CandidateInfo["life"] = Blife_/1.493
            CandidateInfo["DOCA"] = muonsDOCA_
            CandidateInfo["Bip"] = Bip_
            CandidateInfo["angle"] = DirAngle_                      
            CandidateInfo["lessIPSmu"] = min(CandidateInfo["mu1ips"],CandidateInfo["mu2ips"])
            CandidateInfo["C_angle"] =  angleToflight(B1, B2)
            CandidateInfo["mu1_track_Chi2"] = TRCHI2 (mu1)
            CandidateInfo["mu2_track_Chi2"] = TRCHI2 (mu2)

            CandidateInfo["mu1_track_Chi2DoF"] = TRCHI2DOF (mu1)
            CandidateInfo["mu2_track_Chi2DoF"] = TRCHI2DOF (mu2)

            KULLBACK = TINFO ( LHCb.Track.CloneDist , 1.e+9 )
            CandidateInfo["mu1_track_CloneDist"] = KULLBACK(mu1)
            CandidateInfo["mu2_track_CloneDist"] = KULLBACK(mu2)

            CandidateInfo["mu1ismu"] = ISMUON(mu1)
            CandidateInfo["mu2ismu"] = ISMUON(mu2)

            for function in self.extraFunctions:
                function(self, CandidateInfo, Done)
           
            if self.TUP:
                tup = self.nTuple( self.name() )
                for key in CandidateInfo.keys():
                    tup.column(key,CandidateInfo[key])
                tup.write()
           ##  if self.DST:
##                 self.addedKeys = CandidateInfo.keys()
##                 self.addedKeys.sort()
##                 for i in range(len(self.addedKeys)):
##                     j = 500 + i
##                     b.particle().addInfo(j,CandidateInfo[self.addedKeys[i]])
 
            b.save("Bs")

        if self.selected("Bs").empty():
            return SUCCESS
        self.COUNTER["Bender(evts) " + self.decayname] += 1
        #if self.ONLY_MC:
         #   self.COUNTER["MuMuPhotBsel"] += photos
        self.setFilterPassed( True )
        print "Preselected "+self.decayname
        return SUCCESS

    def finalize(self):
        print self.decayname + " counter. \n -----------"
        print self.COUNTER
        return AlgoMC.finalize(self)

