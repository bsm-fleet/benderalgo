#! /usr/bin/env python
### Version for debugging selections Based on BenderBuJPsiK revision 1.9

from SomeUtils.alyabar import *     
from LinkerInstances.eventassoc import * 
import climbing, dv22Tools           #pyDaVinci
from Bender.MainMC import *                
from math import sqrt as s_q_r_t
import GaudiPython 
selectVertexMin = LoKi.SelectVertex.selectMin

###Defining some variables
GBL = GaudiPython.gbl.LHCb
MBu = 5279.000
KMASS = 493.677
MJPsi = 3096.920
MPhi = 1019.460
units = GaudiKernel.SystemOfUnits
c_light = 2.99792458e+8 * units.m /units.s
light_cte = 1000./c_light

class BuJPsiK(AlgoMC):            
    def analyse(self):
        self.COUNTER["EVT"] += 1
        TES = appMgr().evtsvc()
        pvs_ = self.vselect("pvs_", ISPRIMARY)
        if not pvs_.size(): return SUCCESS
      
        if self.MC_INFO:
            BU = climbing.climbing_bujpsik(TES["MC/Particles"].containedObjects(), MCID)
        

        #jpsis = self.select("jpsi", ((ABSID == 443) ))#& (M > MJPsi-60) & (M < MJPsi + 60) ))
        mcmu  = self.mcselect('mcmu'  , '[ B+ =>  ( J/psi(1S) => ^mu+ ^mu- )  K+  ]CC')
        mck   = self.mcselect('mck'   , '[ B+ =>  ( J/psi(1S) =>  mu+  mu- ) ^K+  ]CC' )
        if mcmu  . empty() : return self.Warning ( 'No MC-mu  is found!' , SUCCESS ) # RETURN 
        if mck   . empty() : return self.Warning ( 'No MC-K   is found!' , SUCCESS ) # RETURN 
        matcher = self.mcTruth ( "B+->JPsiK")
        mcMu  = MCTRUTH ( matcher , mcmu   ) 
        mcK   = MCTRUTH ( matcher , mck    ) 

        #muons = self.select ( 'muons' , ('mu+' == ABSID)  &  (mcMu))
        muons = self.select ( 'muons' , ('mu+' == ABSID)  )
        self.select ( "mu+" , muons , 0 < Q )
        self.select ( "mu-" , muons , 0 > Q )
#        kaons = self.select( 'k'  , ('K+'  == ABSID ) & (mcK))
        kaons = self.select( 'k'  , ('K+'  == ABSID ) )
        #print "k's, mu's: ",kaons.size(), muons.size()
        #print "kaons = self.select( 'k'  , ('K+'  == ABSID )",(self.select( 'k'  , ('K+'  == ABSID ))).size()
        #print "muons = self.select ( 'muons' , ('mu+' == ABSID)",(self.select ( 'muons' , ('mu+' == ABSID))).size()

        dimuon = self.loop ( "mu+ mu-" , "J/psi(1S)" )
        for mm in dimuon :
            m12  = mm.mass(1,2) / GeV
            #print "mjpsi ",m12
            #if 3.5 < m12 or 2.5 > m12  : continue
            chi2 = VCHI2( mm )
            if 0 > chi2  : continue
            sc = mm.reFit()
            if sc.isFailure()          : continue
           
            ## mu1 = mm(1)
##             mu2 = mm(2)

##             mc1True = mcMu ( mu1 )
##             mc2True = mcMu ( mu2 )
##             mcTrue  = mcPsi( mm  ) 
##             if mc1True and mc2True and mcTrue :
##                 self.plot ( m12 , " mu mu  mass ", 2.5 , 3.5 )
                
           
            mm.save("jpsi")

        ipcuter = MIP(pvs_,self.geo())
        ip2cuter = MIPCHI2(pvs_,self.geo())
        
        #print jpsis.size(), kaons.size()
        Done = {}   ### This is the dictionary with some objects that might be passed to the functions in the final loop. NOTE that it is defined BEFORE the bsloop
        Done["TES"] = TES
        link = linkedTo(GBL.MCParticle, GBL.Track, "Rec/Track/Best")
        
        #index = 0
        
        #print "[0]", self.selected("jpsi").size(), self.selected("k").size()
        for b in self.loop("jpsi k", "B+"):
            #print "------", index
            #index += 1
            jpsi, kaon = b(1), b(2) # Bu Daughters
        
            if kaon.proto().track().type() != 3: continue
            mu1, mu2 = jpsi.daughters().at(0), jpsi.daughters().at(1) # muons
            key1 = mu1.proto().track().key()
            key2 = mu2.proto().track().key()
            keyk = kaon.proto().track().key()
            if keyk == key1: continue
            if keyk == key2: continue
            #if (self.MASSWINDOW != None) and abs(M(b) - MBu ) > self.MASSWINDOW: continue
            #if abs(M(jpsi) - MJPsi) > 60:
             #   print "Probably Bender JPsi Mass cut is not working at all"
              #  continue
            if self.MC_INFO:
                isMC = 1 
                mcmu1 = link.first(mu1.proto().track())
                mcmu2 = link.first(mu2.proto().track())
                mck = link.first(kaon.proto().track())
                if not BU: isMC = 0
                elif not (mcmu1 and mcmu2 and mck) : isMC = 0
                elif not((MCABSID(mcmu1) ==13) and (MCABSID(mcmu2) == 13) and (MCABSID(mck) == 321)) : isMC = 0
                elif not (mcmu1.mother() and mcmu2.mother() and mck.mother()) :isMC = 0
                elif not ((MCABSID(mcmu1.mother()) == 443) and (MCABSID(mcmu2.mother()) == 443) and (mcmu1.mother().key() == mcmu2.mother().key())): isMC = 0
                elif not (mcmu1.mother().mother() and mcmu2.mother().mother()): isMC = 0
                elif not mcmu1.mother().mother().key() == mcmu2.mother().mother().key() == mck.mother().key() == BU.key(): isMC = 0


            #print "isMC?: ",isMC
            if self.ONLY_MC and not isMC: continue
            PVip = VIP( b.particle(), self.geo())
            PVips2 = VIPCHI2( b.particle(), self.geo())
            JPsiIp = VIP(jpsi, self.geo())
            JPsiIps2 = VIPCHI2(jpsi, self.geo())
            
            PV = selectVertexMin(pvs_, PVips2, (PVips2 >= 0. ))
            ctau = CTAU(PV)
            Blife_ = ctau(b.particle())*light_cte  #in picosecond
            Dis2 = VD( PV )
            Dis2s = VDCHI2( PV )
            PVvec = vector( VX(PV), VY(PV), VZ(PV) )
            SVvec = vector( VX(b), VY(b), VZ(b) )
            JpsiVec = vector( VX(jpsi.endVertex()),  VY(jpsi.endVertex()), VZ(jpsi.endVertex()))
            rSV = SVvec - PVvec
            rJV = JpsiVec - PVvec
            Bp = vector(PX(b), PY(b), PZ(b))
            Jp = vector(PX(jpsi),PY(jpsi),PZ(jpsi))
            kp = vector(PX(kaon),PY(kaon),PZ(kaon))
       
            Bpt = vtmod(Bp)
            Bip = PVip(PV)
            JPip = JPsiIp(PV)
            JPips = JPsiIps2(PV)/psqrt(JPsiIps2(PV))
            f_jpsip = dpr(PVvec,SVvec, Jp)
            def mips(p):
                ipchi2 = ip2cuter(p)
                ips = ipchi2/psqrt(ipchi2)
                return ips


            if f_jpsip : fIPS = Bip*JPips/f_jpsip ## fake IPS for systematic Studies
            else: fIPS = 0.
            Bips = mips(b.particle())
            sigDOF = Dis2(b)
            sigDOF = sigDOF*rSV[2]/abs(rSV[2])
            dDsig = Dis2s(jpsi)  ## SIGNIFICANCE!
            if dDsig<0: continue
            
            dDsig = psqrt(dDsig)*rSV[2]/abs(rSV[2])
            
            #if (self.signedDOFS_Cut != None) and dDsig < self.signedDOFS_Cut: continue
            #if (self.Bips_Cut != None)  and Bips > self.Bips_Cut: continue
            muDOCA = CLAPP (mu1.target(), self.geo())
            DOCA_ = muDOCA(mu2)

            #if (self.Chi2_Cut != None) and jpsi.endVertex().chi2() > self.Chi2_Cut: continue
            
            track1 = mu1.proto().track()
            track2 = mu2.proto().track()

            o1, o2 = track1.position(), track2.position()

            B1 = [ E(jpsi),Jp]
            B2 = [ E(kaon),kp]
            
            Done["Candidate"], Done["mu1"], Done["mu2"] = b.particle(), mu1, mu2
            
            CandidateInfo = {}
            CandidateInfo["isMC"] = isMC
            CandidateInfo["sigDOF"] = sigDOF
            CandidateInfo["JPsi_dis"] = dDsig
            CandidateInfo["evt"] = self.COUNTER["EVT"]
            CandidateInfo["JPsiChi2"] = jpsi.endVertex().chi2()        
            CandidateInfo["Vchi2"] = VCHI2(b.particle().endVertex())
            CandidateInfo["k1ips"] = mips(kaon)
            CandidateInfo["mu1ips"] = mips(mu1)
            CandidateInfo["mu2ips"] = mips(mu2)
            CandidateInfo["JPsiMass"] = M(jpsi)
            CandidateInfo["Bmass"] = M(b)
            CandidateInfo["Bpt"] = Bpt
            CandidateInfo["mu1ismu"] = ISMUON(mu1)
            CandidateInfo["mu2ismu"] = ISMUON(mu2)
            CandidateInfo["kip"] = ipcuter(kaon)
            CandidateInfo["mu1ip"] = ipcuter(mu1)
            CandidateInfo["mu2ip"] = ipcuter(mu2)

            CandidateInfo["mu1pt"] = PT(mu1)
            CandidateInfo["mu2pt"] = PT(mu2)
            CandidateInfo["k1pt"] = PT(kaon)            
            CandidateInfo["mu1ptot"] = P(mu1)
            CandidateInfo["mu2ptot"] = P(mu2)
            CandidateInfo["k1ptot"] = P(kaon)
            CandidateInfo["mu1_eta"] = ETA(mu1)
            CandidateInfo["mu2_eta"] = ETA(mu2)
            CandidateInfo["k1_eta"] = ETA(kaon)
            CandidateInfo["B_eta"] = ETA(b)
            
            CandidateInfo["mu1p1"] = PX(mu1)
            CandidateInfo["mu1p2"] = PY(mu1)
            CandidateInfo["mu1p3"] = PZ(mu1)
            CandidateInfo["mu2p1"] = PX(mu2)
            CandidateInfo["mu2p2"] = PY(mu2)       
            CandidateInfo["mu2p3"] = PZ(mu2)
            CandidateInfo["mu1o1"] = o1.x()
            CandidateInfo["mu1o2"] = o1.y()
            CandidateInfo["mu1o3"] = o1.z()
            CandidateInfo["mu2o1"] = o2.x()
            CandidateInfo["mu2o2"] = o2.y()
            CandidateInfo["mu2o3"] = o2.z()
            CandidateInfo["k1p1"] = PX(kaon)
            CandidateInfo["k1p2"] = PY(kaon)
            CandidateInfo["k1p3"] = PZ(kaon)

            CandidateInfo["QK"] = Q(kaon)          
            CandidateInfo["PIDmu1"] = PIDmu(mu1)   
            CandidateInfo["PIDmu2"] = PIDmu(mu2)   
            CandidateInfo["PIDk"] = PIDK(kaon)    
            CandidateInfo["PIDkp"] = PIDK(kaon) - PIDp(kaon)
            CandidateInfo["SV1"] = VX(b.particle().endVertex())           
            CandidateInfo["SV2"] = VY(b.particle().endVertex())           
            CandidateInfo["SV3"] = VZ(b.particle().endVertex())          
            CandidateInfo["evtNum"] = TES["Rec/Header"].evtNumber()
            CandidateInfo["runNum"] = TES["Rec/Header"].runNumber()          
            CandidateInfo["PV1"] = VX(PV)
            CandidateInfo["PV2"] = VY(PV)
            CandidateInfo["PV3"] = VZ(PV)
            CandidateInfo["Bips"] = Bips
            CandidateInfo["fIPS"] = fIPS
            CandidateInfo["Bip" ] =  Bip
            CandidateInfo["Bdissig"] = psqrt(Dis2s(b.particle()))*rSV[2]/abs(rSV[2])
            CandidateInfo["dDsig"] = dDsig
            CandidateInfo["BmassAlt"] = psqrt(IM2(Jp,kp,MJPsi,KMASS))
            CandidateInfo["C_angle"] = angleToflight(B1, B2)
            CandidateInfo["DOCA"] = DOCA_

            CandidateInfo["mu1_track_Chi2"] = TRCHI2 (mu1)
            CandidateInfo["mu2_track_Chi2"] = TRCHI2 (mu2)
            CandidateInfo["mu1_track_Chi2DoF"] = TRCHI2DOF (mu1)
            CandidateInfo["mu2_track_Chi2DoF"] = TRCHI2DOF (mu2)

            CandidateInfo["kaon_track_Chi2"] = TRCHI2 (kaon)
            CandidateInfo["kaon_track_Chi2DoF"] = TRCHI2DOF (kaon)
              
            KULLBACK = TINFO ( LHCb.Track.CloneDist , 1.e+9 )
            CandidateInfo["mu1_track_CloneDist"] = KULLBACK(mu1)
            CandidateInfo["mu2_track_CloneDist"] = KULLBACK(mu2)
            CandidateInfo["kaon_track_CloneDist"] = KULLBACK(kaon)
               
            for function in self.extraFunctions:
                function(self, CandidateInfo, Done)
                      
            if self.TUP:
                tup = self.nTuple( self.name() )
                for key in CandidateInfo.keys():
                    tup.column(key,CandidateInfo[key])
                tup.write()
                
            if self.DST:
                self.addedKeys = CandidateInfo.keys()
                self.addedKeys.sort()
                for i in range(len(self.addedKeys)):
                    j = 500 + i
                    b.particle().addInfo(j,CandidateInfo[self.addedKeys[i]])
                    
            b.save("B+")

        if self.selected("B+").size():
            print "Bender has found a B+ --> JPsi K+ candidate"
            self.COUNTER["Sel"] += 1
            self.setFilterPassed( True )
        return SUCCESS
    def finalize(self):
        print "B+ --> J/Psi K+ counter. \n -----------"
        print self.COUNTER
        return AlgoMC.finalize(self)

