#! /usr/bin/env python
import sys, os
from Gaudi.Configuration import NTupleSvc
from Configurables import GaudiSequencer, DaVinci, L0Conf, SelDSTWriter, FilterDesktop

bsmumuCounterKeys = ["MuMuCouplesAnalyzed","EVT", "weird"]

################################### Some General Options. Remember to check it

INTERACTIVE = 0

MC_INFO = 1
DST = 0
TUP = 1
TRIGGER = 1
REDOTRIGGER = 0

DOLUMI = False
DebugAlgos = 0

N = -1

Njobmax = 1.e10  ### info only, not real limit. Used to set an offset in the evt no. label

SIMULATION =  1
B2JPSIX_OPT = "Blind"* (not SIMULATION) #0 for rebuild
#VERSION = "stp13"
#VERSION = "stp13_May13" #"mc10d"   ## defines DDDb and CondDB tags for data and MC
#VERSION = "stp12" #"mc10d"   ## defines DDDb and CondDB tags for data and MC
VERSION = "stp17mc10d"   ## defines DDDb and CondDB tags for data and MC

DataDic = {"stp13":"2011","stp13_May13":"2011","stp12":"2010","mc10d":"2010","mc10u":"2010","stp17mc10d":"2010"}




DataType = DataDic[VERSION]  ##### To pass to DV
print DataType, SIMULATION*"MONTE CARLO"

#########################
### ALGORITHMS TO ADD ###
#########################

__BHH = 1
__BsMuMu = 1
__BuJPsiK = 0
__JPsi= 0

__BdJPsiKstar = 0
__Bs2JPsiPhi= 0
__Beta_sLines = 0


###########################
### FOR BATCH MODE ONLY ###
###########################
string = ""
args = sys.argv
for arg in args[1:]:
    if arg in ["-b","-b-"]: continue
    string += "_" + arg

N0 = string.replace("_","")
N0 = int(max(N0,"0"))
    
####################################
### Names for OUTPUT NTuple, Dst ###
####################################


TUPLE_FILE = "~/vol5/NTuples/BDTS" + VERSION + SIMULATION*"MC" + string+".root"

HISTO_FILE = "~/vol5/NTuples/Edi" + VERSION + SIMULATION*"MC" + "_H_"+ string+".root"

#HISTo_FILE = TUPLE_FILE

if TUP:
    
#    NTupleSvc( Output = [ 'T DATAFILE=\'castor:'+os.environ["CASTOR_HOME"]+"/muons/mb" + SIMULATION*"MC"+ string +'.root\' TYP=\'ROOT\' OPT=\'NEW\'' ]) ## To save the NTuple in Castor (a la Elias)
    NTupleSvc( Output =["T DATAFILE='"+ TUPLE_FILE + "' TYP='ROOT' OPT='NEW'"] )


#################
### DATACARDS ###
#################

#datacards = "$BS2MUMUROOT/datacards/stp12/UnblindDatasetsample_CASTOR" + string + ".py"
#datacards = "$BS2MUMUROOT/datacards/stp13/Collision11_mu_May11_20Files.py"
#datacards = "$BS2MUMUROOT/datacards/mc10/Bsmumu_MagAll_CASTOR"+ string + ".py"
#datacards = "$BS2MUMUROOT/datacards/mc10/bbDiMuon_FullMass_CASTOR"+ string + ".py"
#datacards = "/afs/cern.ch/user/d/diegoms/cmtuser/Erasmus_v3r3/Phys/BsMuMuPy/data/MC/mc10/MC10_BuJpsiK_MD.py"
#datacards = "Bsmumu_MagAll_CASTOR.py"
#datacards = "$BS2MUMUROOT/datacards/MC10_sim1_bujpsik_stripped13_0x006d0032_pfn.opts"
datacards = "/afs/cern.ch/user/s/serranoj/public/Bs2MuMu/ForFatima/MC10_sim1_bsmumu_smeared_stripped17_0x006d0032_pfn.opts" #$BS2MUMUROOT/datacards/MC10_sim1_bsmumu_stripped13_0x006d0032_pfn.opts"
#datacards = "$BS2MUMUROOT/datacards/stp13/castor"+ string + ".py"
#datacards = "$BS2MUMUDATACARDSROOT/python/stp13/MagAllBlind_castor"+ string + ".py"
#datacards = "$BS2MUMUDATACARDSROOT/python/mc10/Bu2JPsiPi"+ string + ".py"
#datacards = "/afs/cern.ch/user/a/albrecht/public/forDiego/341fun/blindDST_md_may13.py"
from BenderAlgo.BenderV0 import *
from BenderAlgo.BenderBuJPsiK import *
from BenderAlgo.BenderBdJPsiKst import *
from BenderAlgo.BenderBsJPsiPhi import *
from BenderAlgo.extrafunctions import *
from tmvaops import *
from ROOT import *

import GaudiPython

import Gaudi
from Gaudi.Configuration import *

MCtags = {"sim01reco01":['head-20100624','sim-20100831-vc-md100'],"sim03reco03":['head-20100624','sim-20100831-vc-md100'],"sim07reco06":['head-20101003','sim-20100831-vc-md100'],"mc10d":['head-20101206','sim-20101210-vc-md100'],"mc10u":['head-20101206','sim-20101210-vc-mu100']}
MCtags["stp17mc10d"] = MCtags["mc10d"]

dataTags = {"stp13":['head-20110303','head-20110512'], "stp13_May13":['head-20110303','head-20110505'],"stp12":['head-20101026','head-20101112'],"stp11":['head-20101003','head-20101010']}

Locations = {"mc10d": {}, "stp17":{}, "mc11":{}}
Locations["mc10d"] = {"Bs2MuMu":"AllStreams/Phys/Bs2MuMuWideMassLine", "B2hh": "AllStreams/Phys/Bs2MuMuNoMuIDLine", "Bu2JPsiK": "Phys/PreselBu2JPsiKCommon", "Bd2JPsiKstar":  "Phys/PreselBd2JPsiKstCommon", "Bs2JPsiPhi":"Phys/PreselBs2JPsiPhiCommon"}
#Locations["mc10d"] = {"Bs2MuMu":"AllStreams/Phys/Bs2MuMuWideMassLine", "Bu2JPsiK": "Phys/PreselBu2JPsiKCommon", "Bd2JPsiKstar":  "Phys/PreselBd2JPsiKstCommon", "Bs2JPsiPhi":"Phys/PreselBs2JPsiPhiCommon"}
Locations["stp17Blind"] = {"Bs2MuMu":"BsmumuBlind/Dimuon/Phys/Bs2MuMuLinesWideMassLine",
            "Bu2JPsiK":"BsmumuBlind/Dimuon/Phys/Bs2MuMuLinesBu2JPsiK",
            "Bd2JPsiKst":"BsmumuBlind/Dimuon/Phys/Bs2MuMuLinesBd2JPsiKst",
            "Bs2JPsiPhi":"BsmumuBlind/Dimuon/Phys/Bs2MuMuLinesBs2JPsiPhi",
            "B2hh":"BsmumuBlind/Dimuon/Phys/Bs2MuMuLinesNoMuIDLine",
            "B2hhLoose":"BsmumuBlind/Dimuon/Phys/Bs2MuMuLinesNoMuIDLooseLine"}
Locations["stp17mc10d"] = {"B2hh": "Strip17/Phys/Bs2MuMuLinesNoMuIDLine",
"Bs2MuMu": "Strip17/Phys/Bs2MuMuLinesWideMassLine",
"Bu2JPsiK": "Strip17/Phys/Bs2MuMuLinesBu2JPsiKLine",
"Bd2JPsiKstar" :"Strip17/Phys/Bs2MuMuLinesBd2JPsiKstLine",
"Bs2JPsiPhi" :"Strip17/Phys/Bs2MuMuLinesBs2JPsiPhiLine"
}


bdts = {}

    
def configure():

    
    from Configurables import BsMuMuBDTSelectionToolX as BDTSX
    
    for channel in Locations[VERSION].keys():
        bdts = BDTSX("BDTS_"+channel)
        bdts.Cut = 0  # cut on BDTS
        bdts.VerticesLocation = "/Event/"+Locations[VERSION][channel]+"/_RefitPVs"
        
        bdts.WeightsFile = os.environ["BS2MUMUROOT"]+'/options/TMVAClassification_BDTS_ROOT5_30.weights.xml'   # training process weights
        bdts.FlatHistoFile = os.environ["BS2MUMUROOT"]+'/options/Hflat_BDTS.root'  # root file containing a definition histograme used for flatten BDTS
        #bdts.WeightsFile = "TMVAClassification_BDTS_ROOT5_30.weights.xml"  # training process weights
        #bdts.FlatHistoFile = "Hflat_BDTS.root"  # root file containing a definition histograme used for flatten BDTS


  #  for channel in CONTAINERS:
       
    ################### IMPORTING OPTIONS

    DaVinci().EvtMax = 0    
    DaVinci().DataType = DataType 
    DaVinci().Simulation = SIMULATION
    #DaVinci().L0 = bool(TRIGGER)

    DaVinci().UserAlgorithms = []
    #DaVinci().Hlt = bool(TRIGGER)
    DaVinci().HistogramFile = HISTO_FILE
    DaVinci().Lumi = DOLUMI
    #DaVinci().InputType = "MDST"

    
    if TRIGGER:
        
        L0SelRepSeq = GaudiSequencer("L0SelRepSeq")
        L0SelRepSeq.MeasureTime = True
        from Configurables import L0SelReportsMaker, L0DecReportsMaker
        L0SelRepSeq.Members += [ L0DecReportsMaker(), L0SelReportsMaker() ]
        DaVinci().UserAlgorithms += [ L0SelRepSeq ]
    


    from Configurables import RawBankToSTClusterAlg 
    createITClusters = RawBankToSTClusterAlg("CreateITClusters") 
    createITClusters.DetType     = "IT"

    if SIMULATION:
        DaVinci().DDDBtag = MCtags[VERSION][0]
        DaVinci().CondDBtag = MCtags[VERSION][1]
        #from Configurables import CondDB
        #CondDB().LocalTags['SIMCOND'] = ['simpatch-20100827']

        
    else: ## MagUp
         #from Configurables import CondDB
         #CondDB(UseOracle = True)

         DaVinci().DDDBtag = dataTags[VERSION][0]
         DaVinci().CondDBtag =  dataTags[VERSION][1]
   
    if max(__JPsi , __BuJPsiK , __Bs2JPsiPhi , __BdJPsiKstar):
      
        importOptions("$BS2MUMUROOT/options/DetachedJPsi.py")
        DaVinci().UserAlgorithms += [GaudiSequencer("SeqDetJpsi")]

    if not B2JPSIX_OPT:
        if __BuJPsiK:
            importOptions( "$BS2MUMUROOT/options/DVPreselBu2JPsiK_TrChi2.py" )
            DaVinci().UserAlgorithms += [GaudiSequencer("SeqPreselBu2JPsiKCommon")]

        if __BdJPsiKstar:
            importOptions("$BS2MUMUROOT/options/testKstar.py")
            DaVinci().UserAlgorithms += [GaudiSequencer("SeqtestKstar")]
            importOptions( "$BS2MUMUROOT/options/DVPreselBd2JPsiKstar_TrChi2.py" )
            DaVinci().UserAlgorithms += [GaudiSequencer("SeqPreselBd2JPsiKstCommon") ]

        if __Bs2JPsiPhi:
            importOptions("$BS2MUMUROOT/options/SelPhi.py")
            DaVinci().UserAlgorithms += [GaudiSequencer("SeqSelPhi")]
            importOptions( "$BS2MUMUROOT/options/DVPreselBs2JPsiPhi_TrChi2.py" )
            DaVinci().UserAlgorithms += [GaudiSequencer("SeqPreselBs2JPsiPhiCommon")]

   

   
    #importOptions("$BS2MUMUROOT/options/DVBDTS.py")
    
    importOptions(datacards)  ### Be sure you DO NOT import any other options file after this one. You can overwrite eventselector input.
    ################## END IMPORTING OPTIONS
    DaVinci().applyConf()   
    gaudi = appMgr()
        
    algos = []
    
    if __BsMuMu:
        alg = B2QQ('Bs2MuMu')
        #if SIMULATION: alg.LookIn = "AllStreams/Phys/Bs2MuMuWideMassLine"
        #else: alg.LookIn = "Dimuon/Phys/Bs2MuMuLinesWideMassLine" #"Dimuon/Phys/Bs2MuMuWideMassLine"
        alg.LookIn = Locations[VERSION]["Bs2MuMu"]
        alg.decayname = "Bs-->mumu"
        alg.COUNTER = {}
        alg.COUNTER["Bender(evts) " + alg.decayname] = 0
    
        alg.Sel_Above_GL = 0
        alg.extraFunctions = [trackHits, subdetectorDLLs, geoVarOld, more_muon_things, addSomeGLs, globalEvtVars, CDF_iso_and_Consk, addTMVAs,addBDTS,addBDTS_cx]

        if SIMULATION*MC_INFO: alg.extraFunctions += [BQQMCtruth, mc_geometry]
        for key in bsmumuCounterKeys:
            alg.COUNTER[key] = 0
        algos.append(alg)
        gaudi.addAlgorithm(alg)
      
        alg.InputLocations = ['/Event/Phys/StdLooseMuons' ]   ### Need to be LooseMuons. This is used only for J/Psi veto studies and has nothing to do with what is the Bs2mumu candidate made of 
        
    if __BuJPsiK:
        bu = BuJPsiK("Bu2JPsiK")
        bu.COUNTER= {"EVT":0,"No Online Tracks":0, "No Linked Tracks":0, "Sel":0}
        if not B2JPSIX_OPT :bu.LookIn = "Phys/PreselBu2JPsiKCommon"
        #else: bu.LookIn = B2JPSIX_OPT + "/Phys/SelBu2JPsiK"
        else: bu.LookIn = Locations[VERSION]["Bu2JPsiK"]
        bu.extraFunctions = [more_muon_things, globalEvtVars,addSomeGLs, CDF_iso,addBDTS,addBDTS_cx]
        if SIMULATION: bu.extraFunctions += [Bu2JPsiK_MC, mc_geometry]
        algos.append(bu)
        gaudi.addAlgorithm(bu)
        bu.put_more_cuts = False
        bu.decayname = "B+ --> J/Psi K+"

        bu.InputLocations = []

    if __BdJPsiKstar:
        bd = BdJPsiKstar("Bd2JPsiKstar")
        bd.COUNTER= {"EVT":0,"No Online Tracks":0, "No Linked Tracks":0, "Sel":0}
        if not B2JPSIX_OPT : bd.LookIn = "Phys/PreselBd2JPsiKstCommon"
        #else: bd.LookIn = B2JPSIX_OPT + "/Phys/SelBd2JPsiKstar"
        else: bd.LookIn = Locations[VERSION]["Bd2JPsiKstar"]
        bd.extraFunctions = [more_muon_things, globalEvtVars,addBDTS,addBDTS_cx]
        if SIMULATION: bd.extraFunctions += [B2JpsiKst_MC, mc_geometry]
        algos.append(bd)
        gaudi.addAlgorithm(bd)
        bd.decayname = "B+ --> J/Psi K*"
        bd.InputLocations = []

    if __Bs2JPsiPhi:
        bs = BsJPsiPhi("Bs2JPsiPhi")
        bs.COUNTER= {"EVT":0,"No Online Tracks":0, "No Linked Tracks":0, "Sel":0}
        if not B2JPSIX_OPT : bs.LookIn = "Phys/PreselBs2JPsiPhiCommon"
        #else: bs.LookIn = B2JPSIX_OPT + "/Phys/SelBs2JPsiPhi"
        else: bs.LookIn = Locations[VERSION]["Bs2JPsiPhi"]
        bs.extraFunctions = [more_muon_things, globalEvtVars,addBDTS,addBDTS_cx]
        algos.append(bs)
        gaudi.addAlgorithm(bs)
        bs.decayname = "Bs --> J/Psi Phi"
        bs.InputLocations = []
   
    if __BHH:
        bhh = B2QQ("B2hh")
        bhh.decayname = "B --> h+ h-"
        algos.append(bhh)
        bhh.COUNTER = {}
        bhh.COUNTER["Bender(evts) " + bhh.decayname] = 0
        #if SIMULATION: bhh.LookIn = "AllStreams/Phys/Bs2MuMuNoMuIDLine"
        #else: bhh.LookIn = "Dimuon/Phys/Bs2MuMuLinesNoMuIDLine"#Dimuon/Phys/Bs2MuMuNoMuIDLine"
        bhh.LookIn= Locations[VERSION]["B2hh"]
        bhh.Sel_Above_GL = 0
        bhh.extraFunctions = [trackHits, subdetectorDLLs, geoVarOld, more_muon_things, addSomeGLs, globalEvtVars, massHypo, CDF_iso_and_Consk , addTMVAs, addBDTS, addBDTS_cx]
        if SIMULATION: bhh.extraFunctions += [BQQMCtruth, mc_geometry]
       
        for key in bsmumuCounterKeys:
            bhh.COUNTER[key] = 0
        gaudi.addAlgorithm(bhh)
        bhh.InputLocations = ['/Event/Phys/StdLooseMuons']  ## Muons needed for strudies of JPsi Veto.
   
    if __JPsi:
        jpsi = B2QQ('JPsi2MuMu')
        jpsi.LookIn = "Phys/DetJpsi"
        jpsi.decayname = "JPsi-->mumu"
        jpsi.COUNTER = {}
        jpsi.COUNTER["Bender(evts) " + alg.decayname] = 0
        jpsi.Sel_Above_GL = 0

        jpsi.extraFunctions = [trackHits, subdetectorDLLs, geoVarOld, more_muon_things, addSomeGLs, globalEvtVars]

        if SIMULATION*MC_INFO: jpsi.extraFunctions += [BQQMCtruth, mc_geometry]
        for key in bsmumuCounterKeys:
            jpsi.COUNTER[key] = 0
        algos.append(jpsi)
        gaudi.addAlgorithm(jpsi)
      
        jpsi.InputLocations = ['/Event/Phys/StdLooseMuons' ]
   
        
        #bdts.WeightsFile = "TMVAClassification_BDTS_ROOT5_30.weights.xml"  # training process weights

    

    gaudi.initialize()
    from BenderAlgo import PIDcalTools
 
    ########################
    ##COMMON ATRIBUTES ####
    ######################
   
    for algo in algos:
        algo.MC_INFO = SIMULATION*MC_INFO
        algo.TRIGGER = TRIGGER
        algo.PIDcalTools = PIDcalTools
        algo.NTupleLUN = "T"
        algo.addedKeys = []
        #algo.runinfo = runinfo
        algo.DST = DST
        algo.TUP = TUP
        algo.COUNTER['negSq'] = 0
        algo.COUNTER["weird"] = 0
        algo.DEBUG = DebugAlgos ### Do not define global DEBUG
       
        algo.evt_of = Njobmax*N0

        algo.l0BankDecoder=algo.tool(cpp.IL0DUFromRawTool,'L0DUFromRawTool')
        algo.rawBankDecoder=algo.tool(cpp.IOTRawBankDecoder,'OTRawBankDecoder')

        algo.PVRefitter = algo.tool(cpp.IPVReFitter,"AdaptivePVReFitter")
        algo.LifeFitter = algo.tool(cpp.ILifetimeFitter,"PropertimeFitter")
        algo.Geom =  algo.tool(cpp.IDistanceCalculator,"LoKi::DistanceCalculator")
       
        #bdts.FlatHistoFile = "Hflat_BDTS.root"  # root file containing a definition histograme used for flatten BDTS

        algo.BDTSTool = gaudi.toolsvc().create("BsMuMuBDTSelectionToolX/BDTS_"+algo.name(),interface="IBsMuMuBDTSelectionTool")


        if TRIGGER:
            algo.tistostool = algo.tool( cpp.ITriggerTisTos , 'TriggerTisTos')
            algo.tisTosToolL0= algo.tool(cpp.ITriggerTisTos , 'L0TriggerTisTos')
            algo.extraFunctions.append(triggerBlock)
            
            import Bs2MuMu.triggerDecisionLists as trigList
            algo.l0List = trigList.l0List_2011
            algo.hlt1List = trigList.hlt1List_2011
            algo.hlt2List = trigList.hlt2List_2011
            
configure()

gaudi = appMgr()

if INTERACTIVE:
    for i in range(INTERACTIVE):
        gaudi.run(1)
        TES = gaudi.evtsvc()
       
else:
        
    gaudi.run(N)
    gaudi.stop()
    #if DST: gaudi.service( 'PoolRootKeyEvtCnvSvc').finalize() # This line prevents from loosing your output dst if something doesn't know how to finalize...
    gaudi.finalize()
#
#len(TES["Phys/B2HHFilterSel/Particles"])
